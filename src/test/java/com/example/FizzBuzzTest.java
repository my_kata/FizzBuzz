package com.example;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FizzBuzzTest {

    FizzBuzz fizzBuzz;

    @Before
    public void setup() {
        fizzBuzz = new FizzBuzz();
    }

    @Test
    public void testNumber() {
        String returnedNumber = fizzBuzz.show(1);
        assertEquals("1", returnedNumber);
    }

    @Test
    public void testFizz() {
        String returnedNumber = fizzBuzz.show(3);
        assertEquals("Fizz", returnedNumber);
    }

    @Test
    public void testFizzForNumbersDivisibleBy3() {
        String returnedNumber = fizzBuzz.show(6);
        assertEquals("Fizz", returnedNumber);
    }

    @Test
    public void testBuzz() {
        String returnedNumber = fizzBuzz.show(5);
        assertEquals("Buzz", returnedNumber);
    }

    @Test
    public void testBuzzForNumbersDivisibleBy5() {
        String returnedNumber = fizzBuzz.show(10);
        assertEquals("Buzz", returnedNumber);
    }

    @Test
    public void testFizzBuzz() {
        String returnedNumber = fizzBuzz.show(15);
        assertEquals("FizzBuzz", returnedNumber);
    }

}

