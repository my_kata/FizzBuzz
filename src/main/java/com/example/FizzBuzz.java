package com.example;

public class FizzBuzz {
    public String show(int number) {
        if (isDivisibleBy3(number) && isDivisibleBy5(number)) {
            return "FizzBuzz";
        } else if (isDivisibleBy3(number)) {
            return "Fizz";
        } else if (isDivisibleBy5(number)) {
            return "Buzz";
        }
        return String.valueOf(number);
    }

    private static boolean isDivisibleBy5(int number) {
        return number % 5 == 0;
    }

    private static boolean isDivisibleBy3(int number) {
        return number % 3 == 0;
    }
}
