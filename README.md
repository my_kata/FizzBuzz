# FizzBuzz



## Getting started
---

We're given a number in the form of an integer n.

Write a function that returns the string representation of all numbers from 1 to n based on the following rules:

- If it's a multiple of 3, represent it as "fizz".

- If it's a multiple of 5, represent it as "buzz".

- If it's a multiple of both 3 and 5, represent it as "fizzbuzz".

- If it's neither, just return the number itself.

## Constraints
---

- Maximum value of Integer n <= 1000

- n will always be a positive integer, but can be 0

- Expected time complexity : O(n)

- Expected space complexity : O(1)